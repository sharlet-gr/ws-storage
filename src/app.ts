'use strict';
require('dotenv').config();

// Load koa application
import koa from "koa";
import logger from "koa-logger";
import raven from 'raven';
import Sequelize from 'sequelize';
import i18n from "./assets/messages";
import action from "./routes/action";
import trigger from "./routes/trigger";

// Libraries
const cors = require('koa-cors');
const bodyParser = require('koa-bodyparser');
const Stores = require('koa2-ratelimit').Stores;
const RateLimit = require('koa2-ratelimit').RateLimit;
const parameters = require('./../package.json');
const router = require('koa-router');

// Init application
const app: any = new koa();

// Cors
app.use(cors({ methods: 'GET,HEAD,PUT,POST,PATCH,DELETE' }));

app.use(bodyParser());

// Set env
app.env = process.env.ENV;

// Logger
/* istanbul ignore next */
if (((app.env !== 'local') && (app.env !== 'integration')) || (process.env.LOG === 'true')) {
  app.use(logger());
}

// Get database from ENV
/* istanbul ignore next */
const databasePath: any = process.env.CI
  ? 'postgres://postgres@localhost:5432/postgres'
  : parameters['aws-ratelimits'][app.env.toLocaleUpperCase()];

const sequelize: any = new Sequelize(databasePath, {
  define: { charset: 'utf8', collate: 'utf8_general_ci' },
  logging: false,
  operatorsAliases: false,
});

sequelize
  .authenticate()
  .then(async () => {
    /* istanbul ignore if */
    if (app.env !== 'local') {
      console.info(`Connection has been established successfully for rate limit: ${databasePath}`);
    }
  }).catch((err: any) => {
  /* istanbul ignore next */
  console.info(`Unable to connect to the database for rate limit: ${databasePath}, ${err}`);
});

// Fix RateLimit global variable !
const globalAny: any = global;
globalAny.sequelize = sequelize;

// Rate limits
const limiter = RateLimit.middleware({
  interval: 1000, // 1 second
  max: 100, // limit each IP to 100 requests per interval (1s)
  timeWait: 5 * 1000, // 5 seconds
  message: i18n.t('ERROR-429'),
  store: new Stores.Sequelize(globalAny.sequelize, {
    tableName: `ratelimits-${parameters.name}`, // table to manage the middleware
    tableAbuseName: `ratelimitsabuses-${parameters.name}`, // table to store the history of abuses in.
  }),
});

// apply to all Rate limits requests
app.use(limiter);

/* istanbul ignore if */
if (parameters.sentry) {
  raven.config(parameters.sentry).install();
  app.on('error', (err: any) => {
    raven.captureException(err, (_, eventId) => {
      console.info('Reported error', eventId);
    });
  });
}

const _router = new router();
_router.post('/', action);
_router.post('/triggers/:id', trigger);

app
  .use(_router.routes())
  .use(_router.allowedMethods());

export default app;
