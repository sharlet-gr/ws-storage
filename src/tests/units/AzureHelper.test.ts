import assert from "assert";
import fs from "fs";
import AzureHelper from "../../helpers/AzureHelper";

describe("Storage - Azure", () => {
  const commonObj = {
    credentials: {
      accountName: 'testyoungapp',
      accountKey: 'YBK6zRRqsBuNKShsRDKDnbdxp/vTKERnZysVXbKLw/zhS0LyCjbnT2X3iSEwRHG6LgM6D1sxM55m5r6w44XUTw==',
    },
    container: 'ws-storage-testing-youngapp-co',
    blob: 'test',
  };

  const azure = new AzureHelper();

  after(() => {
    fs.unlinkSync(`${__dirname}/get-test.jpg`);
  });

  it("U-TEST-1 - Create object", async () => {
    const fileBuffer = fs.readFileSync(`${__dirname}/test.jpg`);
    const createResponse = await azure.create({
      ...commonObj,
      buffer: fileBuffer,
    });

    assert.equal(createResponse.success, true);
    assert.equal(createResponse.response.statusCode, 201);
  });

  it("U-TEST-2 - Get object", async () => {
    const getResponse = await azure.get({
      ...commonObj,
      fileName: `${__dirname}/get-test.jpg`,
    });

    assert.equal(getResponse.success, true);
    assert.equal(fs.existsSync(`${__dirname}/get-test.jpg`), true);
    assert.equal(getResponse.response.statusCode, 200);
  });

  it("U-TEST-3 - Delete object", async () => {
    const deleteResponse = await azure.delete({
      ...commonObj,
    });

    assert.equal(deleteResponse.success, true);
    assert.equal(deleteResponse.result, true);
    assert.equal(deleteResponse.response.statusCode, 202);
  });
});
