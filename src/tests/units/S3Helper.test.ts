import assert from "assert";
import S3Helper from "../../helpers/S3Helper";

describe("Storage - S3", () => {
  const commonObj = {
    credentials: {
      accessId: 'AKIAJPSR5HCRCGAUX5IA',
      accessSecret: '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',
      region: 'eu-west-3',
    },
    bucket: 'ws-storage-testing.youngapp.co',
  };
  let createdObjectETag: string;

  const S3 = new S3Helper();

  it("U-TEST-1 - Create object", async () => {
    const createResponse = await S3.create({
      ...commonObj,
      body: 'Test Content',
      key: 'testObject',
    });

    assert.equal(createResponse.success, true);
    assert.notEqual(createResponse.data.ETag, undefined);

    // Required for remaining tests
    createdObjectETag = createResponse.data.ETag;
  });

  it("U-TEST-2 - Get object", async () => {
    const getResponse = await S3.get({
      ...commonObj,
      key: 'testObject',
    });

    assert.equal(getResponse.success, true);
    assert.equal(getResponse.data.ETag, createdObjectETag);
  });

  it("U-TEST-3 - Delete object", async () => {
    const deleteResponse = await S3.delete({
      ...commonObj,
      key: 'testObject',
    });

    assert.equal(deleteResponse.success, true);
  });
});
