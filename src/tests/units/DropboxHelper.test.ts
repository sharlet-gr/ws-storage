import assert from "assert";
import fs from "fs";
import DropboxHelper from "../../helpers/DropboxHelper";

describe("Storage - Dropbox", () => {
  const commonObj = {
    credentials: {
      accessToken: '3nOkwz7OtMAAAAAAAAAEaHRg8f93aLmlLRiaa-lwrQhppobSSx9KSMBtUKC1AbhK',
    },
    path: `/test.jpg`,
  };

  const dropbox = new DropboxHelper();

  it("U-TEST-1 - Create file", async () => {
    const fileBuffer = fs.readFileSync(`${__dirname}/test.jpg`);
    const createResponse = await dropbox.create({
      ...commonObj,
      buffer: fileBuffer,
    });

    assert.equal(createResponse.success, true);
    assert.equal(createResponse.result.name, 'test.jpg');
    assert.equal(createResponse.result.path_display, '/test.jpg');
  });

  it("U-TEST-2 - Get file", async () => {
    const getResponse = await dropbox.get({
      ...commonObj,
    });
    assert.equal(getResponse.success, true);
    assert.equal(getResponse.result.name, 'test.jpg');
    assert.equal(getResponse.result.path_display, '/test.jpg');
  });

  it("U-TEST-3 - Delete file", async () => {
    const deleteResponse = await dropbox.delete({
      ...commonObj,
    });

    assert.equal(deleteResponse.success, true);
    assert.equal(deleteResponse.result.name, 'test.jpg');
    assert.equal(deleteResponse.result.path_display, '/test.jpg');
  });
});
