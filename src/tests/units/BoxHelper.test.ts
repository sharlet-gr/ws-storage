import assert from "assert";
import fs from "fs";
import BoxHelper from "../../helpers/BoxHelper";

describe("Storage - Box", () => {
  const commonObj = {
    credentials: {
      clientId: '4zu0gyvy3649veqiwbuj7j6k1munkqf3',
      clientSecret: '5Bx947aXIYJJHpfXVxbjmhU9Vsj3oJ9a',
      publicKeyId: 'wuzwbisd',
      /* tslint:disable-next-line:max-line-length */
      privateKey: '-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI0f9EuXynMQsCAggA\nMBQGCCqGSIb3DQMHBAgvJQ8TV/oQzwSCBMha7cNhkfQwKCb8YaqKXeWXC6zIkLJb\nGHYoJ4fPv+lMs9XPE2mUij9kQ4jzZ+eYic2HVCQ/avJmxNhV1HyTmk06yCW2342x\n2DGlhV24zvy2AFSf3fWNzELOn1Tv9QJhNgDHss42qckAe0KZCaHQFU9ZOgni7+iR\no25dmbHfmfetkY/nFUcQPCQEXKk/KD672LxSP31eZVG7zVUU1XYV4FfdOR630qR/\nPp1yp49zg/T4w3pRhe78ePqzvWeejRAvZRyNONQCX2WB+rGmY+eTb4RsXvCgY81R\n7dqYqRK42DXrthg3ZUZcXQ3ycZgUxoExH0lqw69nyXdsbav0fvrIPmkYaSz/6QRL\nqYF6Ujxe9iDkug8ZvuwjW9lZ1JYHKq13w6FB+igDT6v020NjZdZf1dhnuaj0yqFC\nRbDzX6OA0EhI796IGG/WMiLIgaVzhZ/VR3avPsAheLBcxzazvfVZC8OHvus02paU\n9jkOwPjA1SwL9JJAKgu8gzjX/VxqyMVhggsmarkt4DWwH5Oii27tSlmmMMIi9qvN\nE3wDL02zNMqvza4l3J0I3tivXtkLhoUTnkX6p4z6oJ7UDrprchtOBceDsZLWbH46\nuMsGxr5Tr6fRwnxh5YScSa70I01iRBEpx3Un1IjiQMm17l9xoHi21zugvM+exGjl\n3fkcQip33+qKe5RXJVmEjVNK86t7yMvzcKHdwA2wWrh7naQPRZ+4sPG6kpDtpI1L\nOPWEFTgeIkHg+hE4YesBKmR/BAuN69GBRx+5Q4txdyuIQAHbPshaee7UAJOA+F5e\nc0z90xQFL+CYvbN3WWXx8n2Ou7wQyIZC0BJNOgyKz7ZkLoKRq1hA272YE94HOzq4\nS5ADRv+OFE21keUKTM+wWVew2hLOtfEPJDUXKxxsCqTbPHcf6ZLNcg1T5riu2BKt\nN9U7SLsCiuwz5fiomhK/ocg3+vhKyZyUW4qW+9fvfKkjrz5sGTYVU8FaVgfLvnUd\nbu/6+a49DUeuPtpq7vbEJgWQl3PBEn9GlzrmRl979z1xaEqSTlL4s+pYt+M4LtFE\nnXm8AO607jjjVEmoo10w/LdaXYwwQAQtZFHUQbaPS45gAcpNcxCmfUCpvpHp7Hoq\nNSyku4chrHuq263+io5sR8Zkiw9zPV84LHaqYhiS/FV7yvaWxaUrNJpJTGGBvFbC\njlfp8I9slcbR06I3FjwLayhSXIi/Cxy3tP/CGgmU8vGoxkXeeDmE5Ck01iO0bI81\nt9i4q23f38FHSFVdpSO7ufCAMwMSsfUzqro/bHY/t+n9MOCkdobDzssGDU4mYtMJ\nRqVa7s8rWf+jwxzuwitB6n3w16gKvk9rfs+ul4Odlqy/+JEBKbwLPH3lypW1osUP\nGednhjSZoLSrMgqOozFW9NZf/IOR2NZx9LkSPwdBzLu2B2K3GdYMFhQA5n+w14eB\nNaeP6sVUBRedebVo34gR4zZBjWsCrZ3nEli+Z5KEwXNqY/nF7O1XGKrsmW+hDrEF\nfbJUdyGlNxeU45DS4Wz7fuHX/MDRTMn170ghs8sKr53HgYq+pXd/vaiTHIpLh5LR\nVO4RJCW3ZsbvcFCmASCP7MVzqAlZDn8bumRE1vQbWQg9HJKflOgvaPtkH9C1moWz\nHyw=\n-----END ENCRYPTED PRIVATE KEY-----\n',
      passphrase: '99a5a2ccdda2338affab4d8ce5ebd436',
      enterprise: '152804149',
    },
    folder: 'ws-storage-testing-youngapp-co',
    fileName: 'test',
  };

  const box = new BoxHelper();

  // after(() => {
  //   fs.unlinkSync(`${__dirname}/get-test.jpg`);
  // });

  it("U-TEST-1 - Create object", async () => {
    const fileBuffer = fs.readFileSync(`${__dirname}/test.jpg`);
    const createResponse = await box.create({
      ...commonObj,
      buffer: fileBuffer,
    });

    console.info(createResponse);
    assert.equal(createResponse.success, true);
    // assert.equal(createResponse.response.statusCode, 201);
  });

  // it("U-TEST-2 - Get object", async () => {
  //   const getResponse = await azure.get({
  //     ...commonObj,
  //     fileName: `${__dirname}/get-test.jpg`,
  //   });

  //   assert.equal(getResponse.success, true);
  //   assert.equal(fs.existsSync(`${__dirname}/get-test.jpg`), true);
  //   assert.equal(getResponse.response.statusCode, 200);
  // });

  // it("U-TEST-3 - Delete object", async () => {
  //   const deleteResponse = await azure.delete({
  //     ...commonObj,
  //   });

  //   assert.equal(deleteResponse.success, true);
  //   assert.equal(deleteResponse.result, true);
  //   assert.equal(deleteResponse.response.statusCode, 202);
  // });
});
