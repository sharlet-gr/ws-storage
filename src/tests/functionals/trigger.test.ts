import assert from "assert";
import supertest from "supertest";
import app from "../../app";
const request = supertest.agent(app.callback());

describe("Web Service Trigger WS-STORAGE", () => {
  it("F-TEST-1 - trigger", async () => {
    const res = await request.post("/triggers/myid").set("Accept", "application/json");
    assert.equal(res.body.data, "myid");
  });
});
