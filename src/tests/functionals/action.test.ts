import assert from "assert";
import supertest from "supertest";
import app from "../../app";
const request = supertest.agent(app.callback());

describe("Web Service Action WS-STORAGE", () => {
  it("F-TEST-1 - test", async () => {
    const res = await request.post("/").send({ param: "Hello World!" }).set("Accept", "application/json");
    assert.equal(res.body.data.param, "Hello World!");
  });
});
