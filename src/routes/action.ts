import { Context } from "koa";

/**
 * Action
 * @param ctx ctx KoaJS
 */
export default async (ctx: Context) => {
  const req: any = ctx.request;
  ctx.status = 200;
  ctx.body = { meta: { status: "ok" }, data: req.body };
};
