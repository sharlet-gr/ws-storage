import { Context } from "koa";

/**
 * Action
 * @param ctx ctx KoaJS
 */
export default async (ctx: Context) => {
  const _ctx: any = ctx;
  ctx.status = 200;
  ctx.body = { meta: { status: "ok" }, data: _ctx.params.id };
};
