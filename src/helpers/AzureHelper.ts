import Azure from 'azure-storage';
import stream from 'stream';

/**
 * Manage Azure storage
 */
export default class AzureHelper {

  /**
   * Private method to get Azure blob service object
   * @param  {string} $0.accountName Azure storage account name
   * @param  {string} $0.accountKey Azure storage access key
   * @return {Object} Azure blob service object
   */
  private getAzureBlobService({ accountName, accountKey }: any): any {
    return Azure.createBlobService(accountName, accountKey);
  }

  /**
   * Method to upload a file to Azure container
   * @param params
   * @param {Object} params.credentials Credentials required to create Azure blob service object
   * @param {string} params.container Name of the Azure storage container
   * @param {string} params.blob Name of the blob
   * @param {Buffer} params.buffer File buffer to be uploaded
   * @return {Promise}
   */
  public async create(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const azureBlobService = this.getAzureBlobService(params.credentials);
      // create container if it doesn't already exist
      azureBlobService.createContainerIfNotExists(params.container, {
        publicAccessLevel: 'blob',
      }, (createContainerError: Error) => {
        /* istanbul ignore if */
        if (createContainerError) { return reject(createContainerError); }

        // create readable stream from buffer
        const fileStream = new stream.Readable();
        fileStream.push(params.buffer);
        fileStream.push(null);

        // upload file
        azureBlobService.createBlockBlobFromStream(
          params.container,
          params.blob,
          fileStream,
          params.buffer.length,
          (createError: Error, createResult: any, createResponse: any) => {
            /* istanbul ignore if */
            if (createError) { return reject(createError); }
            return resolve({ success: true, result: createResult, response: createResponse });
          },
        );
      });
    });
  }

  /**
   * Method to get a file from Azure container
   * @param params
   * @param {Object} params.credentials Credentials required to create Azure blob service object
   * @param {string} params.container Name of the Azure storage container
   * @param {string} params.blob Name of the blob
   * @param {string} params.fileName The local path to the file to be downloaded
   * @return {Promise}
   */
  public async get(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const azureBlobService = this.getAzureBlobService(params.credentials);
      azureBlobService.getBlobToLocalFile(
        params.container,
        params.blob,
        params.fileName,
        (getError: Error, getResult: any, getResponse: any) => {
          /* istanbul ignore if */
          if (getError) { return reject(getError); }
          return resolve({ success: true, result: getResult, response: getResponse });
        },
      );
    });
  }

  /**
   * Method to delete a file from Azure container
   * @param params
   * @param {Object} params.credentials Credentials required to create Azure blob service object
   * @param {string} params.container Name of the Azure storage container
   * @param {string} params.blob Name of the blob
   * @return {Promise}
   */
  public async delete(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const azureBlobService = this.getAzureBlobService(params.credentials);
      azureBlobService.deleteBlobIfExists(
        params.container,
        params.blob,
        (deleteError: Error, deleteResult: any, deleteResponse: any) => {
          /* istanbul ignore if */
          if (deleteError) { return reject(deleteError); }
          return resolve({ success: true, result: deleteResult, response: deleteResponse });
        },
      );
    });
  }
}
