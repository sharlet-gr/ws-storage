const Dropbox = require('dropbox').Dropbox;
import fetch from 'node-fetch';

/**
 * Manage Dropbox storage
 */
export default class DropboxHelper {

  /**
   * Private method to get Dropbox object
   * @param  {string} $0.accessToken Dropbox access token
   * @return {Object} Dropbox object
   */
  private getDropboxObject({ accessToken }: any): any {
    return new Dropbox({ accessToken, fetch });
  }

  /**
   * Method to upload a file to Dropbox
   * @param params
   * @param {Object} params.credentials Credentials required to create Azure blob service object
   * @param {Buffer} params.buffer File buffer to be uploaded
   * @param {string} params.path Path in the user's Dropbox to save the file
   * @return {Promise}
   */
  public async create(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const dropboxObject = this.getDropboxObject(params.credentials);
      // upload file
      dropboxObject.filesUpload({
        path: params.path,
        contents: params.buffer,
      }).then((createResponse: any) => {
        return resolve({ success: true, result: createResponse });
      }).catch(/* istanbul ignore next */(createError: Error) => {
        return reject(createError);
      });
    });
  }

  /**
   * Method to get a file from Dropbox
   * @param params
   * @param {Object} params.credentials Credentials required to create Azure blob service object
   * @param {string} params.path Path in the user's Dropbox to save the file
   * @return {Promise}
   */
  public async get(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const dropboxObject = this.getDropboxObject(params.credentials);
      dropboxObject.filesDownload({
        path: params.path,
      }).then((getResponse: any) => {
        return resolve({ success: true, result: getResponse });
      }).catch(/* istanbul ignore next */(getError: Error) => {
        return reject(getError);
      });
    });
  }

  /**
   * Method to delete a file from Dropbox
   * @param params
   * @param {Object} params.credentials Credentials required to create Azure blob service object
   * @param {string} params.path Path in the user's Dropbox to save the file
   * @return {Promise}
   */
  public async delete(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const dropboxObject = this.getDropboxObject(params.credentials);
      dropboxObject.filesDelete({
        path: params.path,
      }).then((deleteResponse: any) => {
        return resolve({ success: true, result: deleteResponse });
      }).catch(/* istanbul ignore next */(deleteError: Error) => {
        return reject(deleteError);
      });
    });
  }
}
