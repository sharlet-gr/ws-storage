import BoxSDK from 'box-node-sdk';
import fs from 'fs';
// import stream from 'stream';

/**
 * Manage Box storage
 */
export default class BoxHelper {

  /**
   * Private method to get Box client object
   * @param  {string} $0.clientId Box client ID
   * @param  {string} $0.clientSecret Box client secret
   * @param  {string} $0.publicKeyId Box public key ID
   * @param  {string} $0.privateKey Box private key
   * @param  {string} $0.passphrase Box passphrase
   * @param  {string} $0.enterprise Box app enterprise ID
   * @return {Object} Box client object
   */
  private getBoxClient({ }: any): any {
    // const sdk = new BoxSDK({
    //   clientID: clientId,
    //   clientSecret,
    //   appAuth: {
    //     keyID: publicKeyId,
    //     privateKey,
    //     passphrase,
    //   },
    // });
    const sdk = new BoxSDK({
      clientID: "4zu0gyvy3649veqiwbuj7j6k1munkqf3",
      clientSecret: "5Bx947aXIYJJHpfXVxbjmhU9Vsj3oJ9a",
      appAuth: {
        keyID: "wuzwbisd",
        /* tslint:disable-next-line:max-line-length */
        privateKey: "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI0f9EuXynMQsCAggA\nMBQGCCqGSIb3DQMHBAgvJQ8TV/oQzwSCBMha7cNhkfQwKCb8YaqKXeWXC6zIkLJb\nGHYoJ4fPv+lMs9XPE2mUij9kQ4jzZ+eYic2HVCQ/avJmxNhV1HyTmk06yCW2342x\n2DGlhV24zvy2AFSf3fWNzELOn1Tv9QJhNgDHss42qckAe0KZCaHQFU9ZOgni7+iR\no25dmbHfmfetkY/nFUcQPCQEXKk/KD672LxSP31eZVG7zVUU1XYV4FfdOR630qR/\nPp1yp49zg/T4w3pRhe78ePqzvWeejRAvZRyNONQCX2WB+rGmY+eTb4RsXvCgY81R\n7dqYqRK42DXrthg3ZUZcXQ3ycZgUxoExH0lqw69nyXdsbav0fvrIPmkYaSz/6QRL\nqYF6Ujxe9iDkug8ZvuwjW9lZ1JYHKq13w6FB+igDT6v020NjZdZf1dhnuaj0yqFC\nRbDzX6OA0EhI796IGG/WMiLIgaVzhZ/VR3avPsAheLBcxzazvfVZC8OHvus02paU\n9jkOwPjA1SwL9JJAKgu8gzjX/VxqyMVhggsmarkt4DWwH5Oii27tSlmmMMIi9qvN\nE3wDL02zNMqvza4l3J0I3tivXtkLhoUTnkX6p4z6oJ7UDrprchtOBceDsZLWbH46\nuMsGxr5Tr6fRwnxh5YScSa70I01iRBEpx3Un1IjiQMm17l9xoHi21zugvM+exGjl\n3fkcQip33+qKe5RXJVmEjVNK86t7yMvzcKHdwA2wWrh7naQPRZ+4sPG6kpDtpI1L\nOPWEFTgeIkHg+hE4YesBKmR/BAuN69GBRx+5Q4txdyuIQAHbPshaee7UAJOA+F5e\nc0z90xQFL+CYvbN3WWXx8n2Ou7wQyIZC0BJNOgyKz7ZkLoKRq1hA272YE94HOzq4\nS5ADRv+OFE21keUKTM+wWVew2hLOtfEPJDUXKxxsCqTbPHcf6ZLNcg1T5riu2BKt\nN9U7SLsCiuwz5fiomhK/ocg3+vhKyZyUW4qW+9fvfKkjrz5sGTYVU8FaVgfLvnUd\nbu/6+a49DUeuPtpq7vbEJgWQl3PBEn9GlzrmRl979z1xaEqSTlL4s+pYt+M4LtFE\nnXm8AO607jjjVEmoo10w/LdaXYwwQAQtZFHUQbaPS45gAcpNcxCmfUCpvpHp7Hoq\nNSyku4chrHuq263+io5sR8Zkiw9zPV84LHaqYhiS/FV7yvaWxaUrNJpJTGGBvFbC\njlfp8I9slcbR06I3FjwLayhSXIi/Cxy3tP/CGgmU8vGoxkXeeDmE5Ck01iO0bI81\nt9i4q23f38FHSFVdpSO7ufCAMwMSsfUzqro/bHY/t+n9MOCkdobDzssGDU4mYtMJ\nRqVa7s8rWf+jwxzuwitB6n3w16gKvk9rfs+ul4Odlqy/+JEBKbwLPH3lypW1osUP\nGednhjSZoLSrMgqOozFW9NZf/IOR2NZx9LkSPwdBzLu2B2K3GdYMFhQA5n+w14eB\nNaeP6sVUBRedebVo34gR4zZBjWsCrZ3nEli+Z5KEwXNqY/nF7O1XGKrsmW+hDrEF\nfbJUdyGlNxeU45DS4Wz7fuHX/MDRTMn170ghs8sKr53HgYq+pXd/vaiTHIpLh5LR\nVO4RJCW3ZsbvcFCmASCP7MVzqAlZDn8bumRE1vQbWQg9HJKflOgvaPtkH9C1moWz\nHyw=\n-----END ENCRYPTED PRIVATE KEY-----\n",
        passphrase: "99a5a2ccdda2338affab4d8ce5ebd436",
      },
    });
    return sdk.getAppAuthClient('enterprise', '152804149');
  }

  /**
   * Method to upload a file to Box
   * @param params
   * @param {Object} params.credentials Credentials required to create Box client object
   * @param {Buffer} params.buffer File buffer to be uploaded
   * @param {string} params.folder Name of the folder to upload the file in
   * @param {string} params.fileName Name of the file
   * @return {Promise}
   */
  public async create(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      // const boxClient = this.getBoxClient(params.credentials);
      // // create readable stream from buffer
      // const fileStream = new stream.Readable();
      // fileStream.push(params.buffer);
      // fileStream.push(null);

      // // upload file
      // boxClient.files.uploadFile(
      //   params.folder,
      //   params.fileName,
      //   fileStream,
      //   (createError: Error, createResponse: any) => {
      //     /* istanbul ignore if */
      //     if (createError) { console.info('66666666666666666'); return reject(createError); }
      //     return resolve({ success: true, response: createResponse });
      //   },
      // );
      const boxClient = this.getBoxClient(params.credentials);
      const fileData = fs.createReadStream(`/home/rails/shw/ws-storage/src/tests/units/test.txt`);

      // upload file
      boxClient.files.uploadFile(
        '123',
        'test.txt',
        fileData,
        (createError: Error, createResponse: any) => {
          /* istanbul ignore if */
          if (createError) { console.info('66666666666666666'); return reject(createError); }
          return resolve({ success: true, response: createResponse });
        },
      );
    });
  }

  // /**
  //  * Method to get a file from Azure container
  //  * @param params
  //  * @param {Object} params.credentials Credentials required to create Azure blob service object
  //  * @param {string} params.container Name of the Azure storage container
  //  * @param {string} params.blob Name of the blob
  //  * @param {string} params.fileName The local path to the file to be downloaded
  //  * @return {Promise}
  //  */
  // public async get(params: any): Promise<any> {
  //   return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
  //     const azureBlobService = this.getAzureBlobService(params.credentials);
  //     azureBlobService.getBlobToLocalFile(
  //       params.container,
  //       params.blob,
  //       params.fileName,
  //       (getError: Error, getResult: any, getResponse: any) => {
  //         /* istanbul ignore if */
  //         if (getError) { return reject(getError); }
  //         return resolve({ success: true, result: getResult, response: getResponse });
  //       },
  //     );
  //   });
  // }

  // /**
  //  * Method to delete a file from Azure container
  //  * @param params
  //  * @param {Object} params.credentials Credentials required to create Azure blob service object
  //  * @param {string} params.container Name of the Azure storage container
  //  * @param {string} params.blob Name of the blob
  //  * @return {Promise}
  //  */
  // public async delete(params: any): Promise<any> {
  //   return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
  //     const azureBlobService = this.getAzureBlobService(params.credentials);
  //     azureBlobService.deleteBlobIfExists(
  //       params.container,
  //       params.blob,
  //       (deleteError: Error, deleteResult: any, deleteResponse: any) => {
  //         /* istanbul ignore if */
  //         if (deleteError) { return reject(deleteError); }
  //         return resolve({ success: true, result: deleteResult, response: deleteResponse });
  //       },
  //     );
  //   });
  // }
}
