import AWS from 'aws-sdk';

/**
 * Manage AWS S3 storage
 */
export default class S3Helper {

  /**
   * Private method to get S3 object
   * @param  {string} $0.accessId S3 access id
   * @param  {string} $0.accessSecret S3 access secret
   * @param  {string} $0.region S3 region
   * @return {Object} S3 object
   */
  private getS3Object({ accessId, accessSecret, region }: any): any {
    return new AWS.S3({
      accessKeyId: accessId,
      secretAccessKey: accessSecret,
      region,
    });
  }

  /**
   * Method to upload an object to S3 bucket
   * @param params
   * @param {Object} params.credentials Credentials required to create S3 object
   * @param {string} params.bucket Name of the S3 bucket
   * @param {Buffer|Array|stream.Readable|string|Blob} params.body Object data to be uploaded
   * @param {string} params.key Object key
   * @param {string} params.contentEncoding Object key
   * @param {string} params.contentType Object key
   * @return {Promise}
   */
  public async create(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const S3 = this.getS3Object(params.credentials);
      S3.putObject({
        Bucket: params.bucket,
        Body: params.body,
        Key: params.key,
        ContentEncoding: params.contentEncoding,
        ContentType: params.contentType,
      }, (createError: Error, data: any) => {
        /* istanbul ignore if */
        if (createError) { return reject(createError); }
        return resolve({ success: true, data });
      });
    });
  }

  /**
   * Method to get object from S3 bucket
   * @param params
   * @param {Object} params.credentials Credentials required to create S3 object
   * @param {string} params.bucket Name of the S3 bucket
   * @param {string} params.key Object key
   * @return {Promise}
   */
  public async get(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const S3 = this.getS3Object(params.credentials);
      S3.getObject({ Bucket: params.bucket, Key: params.key }, (getError: Error, data: any) => {
        /* istanbul ignore if */
        if (getError) { reject(getError); }
        return resolve({ success: true, data });
      });
    });
  }

  /**
   * Method to delete object from S3 bucket
   * @param params
   * @param {Object} params.credentials Credentials required to create S3 object
   * @param {string} params.bucket Name of the S3 bucket
   * @param {string} params.key Object key
   * @return {Promise}
   */
  public async delete(params: any): Promise<any> {
    return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
      const S3 = this.getS3Object(params.credentials);
      S3.deleteObject({ Bucket: params.bucket, Key: params.key }, (deleteError: Error, data: any) => {
        /* istanbul ignore if */
        if (deleteError) { reject(deleteError); }
        return resolve({ success: true, data });
      });
    });
  }

  // /**
  //  * Method to create bucket
  //  * @param params
  //  */
  // public async createBucket(params: any): Promise<any> {
  //   return new Promise(async (resolve: (p: any) => void, reject: (p: any) => void) => {
  //     const S3 = this.getS3Object(params.credentials);
  //     S3.createBucket({ Bucket: 'ws-storage-testing.youngapp.co' }, (err: Error, data: any) => {
  //       if (err) { return reject(err); }
  //       return resolve(data);
  //     });
  //   });
  // }
}
