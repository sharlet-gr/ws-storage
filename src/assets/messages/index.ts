import i18n from 'i18next';

let fr = {};
let en = {};

try {
  fr = require('./locales/locale-fr.json');
} catch (error) {
  // error
}

try {
  en = require('./locales/locale-en.json');
} catch (error) {
  // error
}

i18n
  .init({
    interpolation: { escapeValue: false },
    lng: 'en', // 'en' | 'es'
    // Using simple hardcoded resources for simple example
    resources: {
      fr: { translation: fr },
      en: { translation: en },
    },
  });

export default i18n;