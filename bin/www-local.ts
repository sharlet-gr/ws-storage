#!/usr/bin/env node
require('dotenv').config();

import app from '../src/app';
const parameters = require('./../package.json');

const port = process.env.PORT || 3000;

// // Launch application
app.listen(port, () => {
  console.info(`${String(parameters.name).toUpperCase()} application listening on port ${port}`);
});