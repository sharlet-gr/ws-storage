#!/usr/bin/env node
require('dotenv').config();
const serverless = require('serverless-http');

// // Load locale configuration for use port, env, database
import app from '../src/app';

module.exports.handler = serverless(app);