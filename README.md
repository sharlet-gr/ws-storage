# Welcome to Web Service Young App!
This is a starter help to install project

### Install
```
git clone git+ssh://git@bitbucket.org:youngapp/<name>.git#master
```

### Start
Launch commands
```
yarn run global
yarn install
yarn test
```

### Dependencies
- Koa
- TypeScript
- Sequelize
- Serverless

# Happy Coding!