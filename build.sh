#!/bin/bash

yarn unbuild \
  && BRIDGE_ENV=local tsc --declaration \
  && cp *.json dist/ && yarn gen:graphql \
  && cp -R .graphql dist/ \
  && cp scripts/.env dist/ \
  && cp -R src/studio/assets dist/src/studio/assets \
  && cp -R src/account/assets-templates dist/src/account/assets-templates \
  && cp -R src/core/assets/messages/locales dist/src/core/assets/messages/locales